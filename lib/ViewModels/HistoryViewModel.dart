import 'package:citytaxi/Models/History.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:citytaxi/Pages/RideInfoPage.dart';

class HistoryViewModel extends StatefulWidget {
  final History _history;
  HistoryViewModel(this._history);
  @override
  State<StatefulWidget> createState() {
    return _HistoryViewModelState(_history);
  }
}

class _HistoryViewModelState extends State<HistoryViewModel> {
  final History _history;
  double _tileMarginLeft;
  double _tileMarginTop;
  _HistoryViewModelState(this._history);
  @override
  Widget build(BuildContext context) {
    this._tileMarginLeft = MediaQuery.of(context).size.width * 0.04;
    this._tileMarginTop = MediaQuery.of(context).size.height * 0.013;
    return new Container(
        child: GestureDetector(
      onTap: () => {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => RideInfoPage(_history)))
          },
      child: new Column(
        children: <Widget>[
          new Container(
            margin: EdgeInsets.only(
              left: this._tileMarginLeft,
              top: this._tileMarginTop,
            ),
            alignment: Alignment.centerLeft,
            child: new Text(
              this._history.startAddress,
              textAlign: TextAlign.left,
              style: new TextStyle(
                fontSize: 14,
              ),
            ),
          ),
          new Container(
            margin: EdgeInsets.only(
              left: this._tileMarginLeft,
              top: this._tileMarginTop,
            ),
            alignment: Alignment.centerLeft,
            child: new Text(
              DateFormat('yyyy-MM-dd – kk:mm')
                  .format(DateTime.parse(this._history.time))
                  .toString(),
              textAlign: TextAlign.left,
              style: new TextStyle(
                fontSize: 12,
                color: Colors.black54,
              ),
            ),
          ),
          new Container(
            margin: EdgeInsets.only(
              left: this._tileMarginLeft,
              top: this._tileMarginTop,
            ),
            alignment: Alignment.centerLeft,
            child: new Text(
              this._history.status == 1 ? "Yekunlaşıb" : "Ləğv Olunub",
              textAlign: TextAlign.left,
              style: new TextStyle(
                color: this._history.status == 1 ? Colors.green : Colors.red,
                fontSize: 11,
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
