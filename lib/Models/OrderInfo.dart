class OrderInfo {
  OrderInfo(this.carInfo, this.carNumber, this.carLat, this.carLong,
      this.driverFullname, this.driverPhone);
  String carNumber;
  String carInfo;
  double carLat;
  double carLong;
  String driverFullname;
  String driverPhone;
}
