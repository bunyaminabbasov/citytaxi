class Order {
  Order(this.startStreet, this.endStreet, this.startLat, this.startLong,
      this.endLat, this.endLong, this.money);
  String startStreet;
  String endStreet;
  double startLat;
  double startLong;
  double endLat;
  double endLong;
  double money;
}
