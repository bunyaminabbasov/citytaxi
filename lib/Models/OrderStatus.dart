class OrderStatus {
  OrderStatus(
  this.currentDirection, this.currentLat, this.currentLong, this.status);
  double currentDirection;
  double currentLat;
  double currentLong;
  String status;
}
