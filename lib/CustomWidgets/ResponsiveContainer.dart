import 'package:flutter/material.dart';

class ResponsiveContainer extends StatelessWidget {
  /// The [child] contained by the container.
  ///
  /// If null, and if the [constraints] are unbounded or also null, the
  /// container will expand to fill all available space in its parent, unless
  /// the parent provides unbounded constraints, in which case the container
  /// will attempt to be as small as possible.
  ///
  /// {@macro flutter.widgets.child}
  final Widget child;
  final AlignmentGeometry alignment;
  final EdgeInsetsGeometry padding;
  final EdgeInsetsGeometry margin;
  final double widthPercent;
  final double heightPercent;
  const ResponsiveContainer(
      {Key key,
      this.alignment,
      this.padding,
      this.margin,
      this.child,
      @required this.widthPercent,
      @required this.heightPercent})
      : super(key: key);
  double percent(double value, double porcentagem) {
    return (value * porcentagem) / 100;
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var width = size.width;
    var height = size.height;
    return Container(
      padding: padding,
      margin: margin,
      alignment: alignment,
      child: child,
      width: percent(width, widthPercent),
      height: percent(height, heightPercent),
    );
  }
}
