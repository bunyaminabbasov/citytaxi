import 'package:citytaxi/CustomWidgets/CustomCountryCodePicker.dart';
import 'package:citytaxi/Pages/LoginPage.dart';
import 'package:citytaxi/Static/StaticValue.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gradient_widgets/gradient_widgets.dart';

class PhoneValidate extends StatefulWidget {
  LoginPageState parent;
  PhoneValidate(this.parent);
  @override
  State<StatefulWidget> createState() => _PhoneValidateState(parent);
}

class _PhoneValidateState extends State<PhoneValidate> {
  LoginPageState parent;
  _PhoneValidateState(this.parent);

  final Widget logoSvg =
      new SvgPicture.asset("assets/logo.svg", semanticsLabel: 'Logo');
  TextEditingController numberboxController = new TextEditingController();
  Future<void> _sendSMS() async {
    FocusScope.of(context).requestFocus(new FocusNode());
    var parsedPhone = StaticValue.phoneParser(numberboxController.text);
    StaticValue.amazingLoading(context);
    var result = await StaticValue.checkUser(
        parsedPhone, StaticValue.hasher(parsedPhone));
      Navigator.of(context).pop();

    if (result.toString() == "ok") {
      result = await StaticValue.sendSMS(numberboxController.text,
          StaticValue.hasher("+994" + numberboxController.text));
      if (result == "ok") {
        this.parent.setState(() {
          this.parent.phone = "+994" + numberboxController.text;
          StaticValue.phone = "+994" + numberboxController.text;
          this.parent.childWidget = 1;
        });
      } else {
        await showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: RichText(
                text: TextSpan(
                  children: <TextSpan>[
                    TextSpan(
                      text: 'CityTaxi Corporate ',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
              content: Text("Xəta baş verdi."),
              actions: <Widget>[
                FlatButton(
                  child: Text(
                    "Bəli",
                    style: TextStyle(
                      color: Colors.red,
                    ),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }
    } else {
      await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: RichText(
              text: TextSpan(
                children: <TextSpan>[
                  TextSpan(
                    text: 'CityTaxi Corporate ',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
            content: Text("Xəta baş verdi."),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  "Bəli",
                  style: TextStyle(
                    color: Colors.red,
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }

  @override
  void initState() {
    super.initState();
  }

 
  @override
  Widget build(BuildContext context) {
   
    return new Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: parent.queryData.size.height * 0.05),
            child: logoSvg,
          ),
          Container(
              padding: EdgeInsets.symmetric(
                horizontal: parent.queryData.size.width * 0.1,
                vertical: parent.queryData.size.width * 0.1,
              ),
              child: new Column(
                children: <Widget>[
                  Text(
                    "Nömrənizi təsdiq etmək üçün",
                    style: new TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.grey[600],
                      fontSize: 15,
                    ),
                  ),
                  Text(
                    "SMS kod alacaqsınız",
                    style: new TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.grey[600],
                      fontSize: 15,
                    ),
                  ),
                ],
              )),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: parent.queryData.size.width * 0.1,
            ),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Container(
                  margin: EdgeInsets.only(
                    left: parent.queryData.size.width * 0.03,
                    right: parent.queryData.size.width * 0.01,
                  ),
                  child: new CountryCodePicker(
                    initialSelection: 'az',
                    textStyle: new TextStyle(
                      fontSize: 16,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ),
                new Container(
                  child: new Expanded(
                    child: TextField(
                      cursorWidth: 0,
                      buildCounter: (BuildContext context,
                              {int currentLength,
                              int maxLength,
                              bool isFocused}) =>
                          null,
                      maxLength: 9,
                      controller: numberboxController,
                      keyboardType: TextInputType.number,
                      textInputAction: TextInputAction.done,
                      onEditingComplete: _sendSMS,
                      maxLines: 1,
                      style: new TextStyle(fontSize: 16),
                      decoration: InputDecoration(
                        isDense: false,
                        border: InputBorder.none,
                        hintText: '(_ _) _ _ _  _ _  _ _',
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          new Container(
            padding:
                EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.05),
            child: new GradientButton(
              callback: () async {
                await _sendSMS();
              },
              elevation: 2,
              increaseHeightBy: 0.4,
              increaseWidthBy: 110,
              child: new Text(
                "Növbəti",
                style: new TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w800,
                ),
              ),
              gradient: LinearGradient(
                colors: <Color>[
                  Color.fromRGBO(255, 128, 0, 1),
                  Color.fromRGBO(255, 128, 0, 1),
                ],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ),
            ),
          ),
        ],
      )
    ;
  }
}
