import 'package:citytaxi/Models/History.dart';
import 'package:citytaxi/Pages/HistoryPage.dart';
import 'package:citytaxi/Pages/LoginPage.dart';
import 'package:citytaxi/Static/StaticValue.dart';
import 'package:citytaxi/ViewModels/HistoryViewModel.dart';
import 'package:flutter/material.dart';
import 'package:citytaxi/Static/SharedPreferences.dart' as shared;
import 'CustomDrawer.dart';

class TaxiDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CustomDrawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          Container(
            color: Color.fromRGBO(255, 128, 0, 1),
            height: 80,
            child: DrawerHeader(
              child: Container(
                alignment: Alignment.topLeft,
                child: IconButton(
                  onPressed: (() => {Navigator.of(context).pop()}),
                  icon: Icon(
                    Icons.close,
                    color: Colors.white,
                  ),
                ),
              ),
              decoration: BoxDecoration(
                  color: Color.fromRGBO(255, 128, 0, 1),
                  border: Border.all(color: Color.fromRGBO(255, 128, 0, 1))),
            ),
          ),
          ListTile(
            leading: Container(
              child: Container(
                child: StaticValue.profile,
                margin: EdgeInsets.symmetric(vertical: 5),
              ),
            ),
            title: Text(
              StaticValue.phone!=null?StaticValue.phone:" ",
              style: TextStyle(color: Color.fromRGBO(120, 120, 120, 1)),
            ),
          ),
          Divider(),
          ListTile(
            leading: Container(
              child: Container(
                child: StaticValue.cardrawer,
                margin: EdgeInsets.symmetric(vertical: 5),
              ),
            ),
            title: Text(
              'Gedişlərim',
              style: TextStyle(color: Color.fromRGBO(120, 120, 120, 1)),
            ),
            onTap: () async {
              StaticValue.loading(context);
              List<History> histories = await StaticValue.history(
                  StaticValue.phoneParser(
                      StaticValue.phone.replaceAll("+994", "")),
                  StaticValue.hasher(StaticValue.phoneParser(
                      StaticValue.phone.replaceAll("+994", ""))));
              List<HistoryViewModel> hvs = List<HistoryViewModel>();
              if (histories != null) {
                Navigator.of(context).pop();
                histories.forEach((f) => {hvs.add(new HistoryViewModel(f))});
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => new HistoryPage(hvs)),
                );
              } else {
                Navigator.of(context).pop();
              }
            },
          ),
          ListTile(
            leading: Container(
              child: Container(
                child: StaticValue.logout,
                margin: EdgeInsets.symmetric(vertical: 5),
              ),
            ),
            title: Text(
              'Çıxış',
              style: TextStyle(color: Color.fromRGBO(120, 120, 120, 1)),
            ),
            onTap: () async {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: RichText(
                      text: TextSpan(
                        children: <TextSpan>[
                          TextSpan(
                            text: 'CityTaxi Corporate ',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                    content: Text("Çıxmaq istədiyinizdən əminsinizmi?"),
                    actions: <Widget>[
                      FlatButton(
                        child: Text(
                          "Xeyr",
                          style: TextStyle(
                            color: Colors.black,
                          ),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      FlatButton(
                        child: Text(
                          "Bəli",
                          style: TextStyle(
                            color: Colors.red,
                          ),
                        ),
                        onPressed: () async {
                          await shared.SharedPref.clearV();
                          StaticValue.isCoop=false;
                          StaticValue.isOrdered=false;
                          StaticValue.orderId=null;
                          StaticValue.phone=null;
                          StaticValue.price=null;
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => WillPopScope(
                                        child: LoginPage(),
                                        onWillPop: () {
                                          return;
                                        },
                                      )));
                        },
                      ),
                    ],
                  );
                },
              );
            },
          ),
        ],
      ),
    );
  }
}
