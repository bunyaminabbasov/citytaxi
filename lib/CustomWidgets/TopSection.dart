import 'package:citytaxi/Static/StaticValue.dart';
import 'package:flutter/material.dart';

class TopSection extends StatefulWidget {
  TopSection();
  @override
  State<StatefulWidget> createState() {
    return TopSectionState();
  }
}

class TopSectionState extends State<TopSection> {
  TopSectionState();
  MediaQueryData queryData;
  TextEditingController pickup = new TextEditingController();
  TextEditingController destination = new TextEditingController();
  changeText(String direction, String description) {
    if (direction == "pickup") {
      pickup.text = description;
    } else {
      destination.text = description;
    }
  }
  @override
  Widget build(BuildContext context) {
    pickup.text = StaticValue.pickupStreet;
    destination.text = StaticValue.destinationStreet;
    queryData = MediaQuery.of(context);
    return new Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.symmetric(
            vertical: queryData.size.height * 0.002,
            horizontal: queryData.size.width * 0.01,
          ),
          margin: EdgeInsets.only(
              left: queryData.size.width * 0.12,
              right: queryData.size.width * 0.12,
              top: queryData.size.height * 0.055),
          decoration: BoxDecoration(
            boxShadow: <BoxShadow>[
              new BoxShadow(
                color: Colors.grey[400],
                offset: new Offset(0.2, 0.2),
                blurRadius: 5.0,
              ),
            ],
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
          ),
          child: Container(
            padding: EdgeInsets.only(left: queryData.size.width * 0.03),
            child: InkWell(
              onTap: () async => {
                  },
              child: TextField(
                enabled: false,
                style: TextStyle(fontSize: 14),
                controller: pickup,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    suffixIcon: new Icon(
                      Icons.my_location,
                      color: Color.fromRGBO(255, 128, 0, 1),
                    )),
              ),
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(
            vertical: queryData.size.height * 0.002,
            horizontal: queryData.size.width * 0.01,
          ),
          margin: EdgeInsets.only(
              left: queryData.size.width * 0.12,
              right: queryData.size.width * 0.12,
              top: queryData.size.height * 0.01),
          decoration: BoxDecoration(
            boxShadow: <BoxShadow>[
              new BoxShadow(
                color: Colors.grey[400],
                offset: new Offset(0.2, 0.2),
                blurRadius: 5.0,
              ),
            ],
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
          ),
          child: Container(
              padding: EdgeInsets.only(left: queryData.size.width * 0.03),
              child: InkWell(
                onTap: () async => {
                    },
                child: TextField(
                  enabled: false,
                  style: TextStyle(fontSize: 14),
                  controller: destination,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      suffixIcon: new Icon(
                        Icons.location_on,
                        color: Color.fromRGBO(66, 100, 251, 1),
                      )),
                ),
              )),
        )
      ],
    );
  }
}
