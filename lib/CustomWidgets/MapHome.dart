import 'package:citytaxi/CustomWidgets/CustomAutoComplate.dart';
import 'package:flutter/material.dart';
import 'package:citytaxi/Static/StaticValue.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:gradient_widgets/gradient_widgets.dart';
import 'package:citytaxi/CustomWidgets/TopSection.dart';

class MapHome extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    StaticValue.homeState = MapHomeState();
    return StaticValue.homeState;
  }
}

class MapHomeState extends State<MapHome> {
  MapHomeState() {
    if (StaticValue.pickupStreet != null) {
      currentLocationText = StaticValue.pickupStreet.length > 22
          ? StaticValue.pickupStreet.substring(0, 24) + "..."
          : StaticValue.pickupStreet;
    } else {
      currentLocationText = "Street";
    }
  }
  String currentLocationText;
  @override
  void initState() {
    confirmButtonText = "Başlanğıc yerinizi təsdiqləyin";
    icon = new Icon(
      Icons.my_location,
      color: buttonColor,
    );
    super.initState();
  }

  Future<void> changeCurrentLocStr() async {
    
    try {
      var google =
          await Geocoder.google("AIzaSyBFK4dk83rKGx2KKgt9HjnBGe3HnTjNuGI")
              .findAddressesFromQuery(
                  StaticValue.map.state.center.latitude.toString() +
                      "," +
                      StaticValue.map.state.center.longitude.toString())
              .timeout(Duration(seconds: 2));
      setState(() {
        if (google != null) {
          currentLocationText = google.first.addressLine.toString().length > 22
              ? google.first.addressLine.toString().substring(0, 22) + "..."
              : google.first.addressLine.toString();
        }
        else{
          currentLocationText = "Location";
        }
      });
    } catch (e) {
      setState(() {
        currentLocationText = "Location";
      });
    }
  }

  changeCenterByText() async {
    try {
      var places = await PlacesAutocomplete.show(
          context: context,
          apiKey: "AIzaSyBFK4dk83rKGx2KKgt9HjnBGe3HnTjNuGI",
          mode: Mode.overlay,
          language: "az",
          components: [new Component(Component.country, "az")]);
      StaticValue.amazingLoading(context);
      if(places!=null){
      if (places.description != null) {
        var google =
            await Geocoder.google("AIzaSyBFK4dk83rKGx2KKgt9HjnBGe3HnTjNuGI")
                .findAddressesFromQuery(places.description)
                .timeout(Duration(seconds: 5));
        StaticValue.map.state.changeCenter(google.first.coordinates.toString());
      }
      }
      await changeCurrentLocStr();
    } catch (Exception) {} finally {
      Navigator.of(context).pop();
    }
  }

  Future<void> findAddreses() async {
    var google =
        await Geocoder.google("AIzaSyBFK4dk83rKGx2KKgt9HjnBGe3HnTjNuGI")
            .findAddressesFromQuery(
                StaticValue.map.state.pickup_location.latitude.toString() +
                    "," +
                    StaticValue.map.state.pickup_location.longitude.toString());
    var googleOne =
        await Geocoder.google("AIzaSyBFK4dk83rKGx2KKgt9HjnBGe3HnTjNuGI")
            .findAddressesFromQuery(
                StaticValue.map.state.destination_location.latitude.toString() +
                    "," +
                    StaticValue.map.state.destination_location.longitude
                        .toString());
    StaticValue.pickupStreet = google.first.addressLine.toString();
    StaticValue.destinationStreet = googleOne.first.addressLine.toString();
  }

  Widget icon;
  String confirmButtonText;
  MediaQueryData queryData;
  int counter = 0;
  var buttonColor = Color.fromRGBO(255, 128, 0, 1);
  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);
    return new Stack(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            top: queryData.size.height * 0.2,
            left: queryData.size.width * 0.15,
            right: queryData.size.width * 0.15,
            bottom: queryData.size.height * 0.68,
          ),
          decoration: BoxDecoration(
            boxShadow: <BoxShadow>[
              new BoxShadow(
                color: Colors.grey[400],
                offset: new Offset(0.2, 0.2),
                blurRadius: 5.0,
              ),
            ],
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  onTap: () => {changeCenterByText()},
                  child: new Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(left: queryData.size.width * 0.05),
                    child: new Text(
                      currentLocationText,
                      style: new TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 15),
                  alignment: Alignment.centerRight,
                  child: icon,
                )
              ]),
        ),
        new Container(
          margin: EdgeInsets.all(15),
          alignment: Alignment.bottomRight,
          child: FloatingActionButton(
            onPressed: () {
              StaticValue.map.state.moveMyLocation();
            },
            backgroundColor: Colors.white,
            child: Icon(
              Icons.my_location,
              size: 35,
              color: Color.fromRGBO(255, 128, 0, 1),
            ),
          ),
        ),
        new Container(
          margin: EdgeInsets.only(top: queryData.size.height * 0.45),
          child: new GradientButton(
            callback: () async {
              counter++;
              if (counter == 2) {
                StaticValue.map.state.addMarker();
                StaticValue.amazingLoading(context);
                var mapstate = StaticValue.map.state;
                await StaticValue.calculatePrice(
                        mapstate.pickup_location.latitude,
                        mapstate.pickup_location.longitude,
                        mapstate.destination_location.latitude,
                        mapstate.destination_location.longitude)
                    .then((e) => {
                          StaticValue.price = e,
                        });
                await findAddreses();
                Navigator.of(context).pop();
                StaticValue.mapPageState.removeWidget(new TopSection(), 1);
              } else {
                setState(() => {
                      buttonColor = Color.fromRGBO(66, 100, 251, 1),
                      confirmButtonText = "Dayanacaq yerinizi təsdiqləyin",
                      icon = new Icon(
                        Icons.location_on,
                        color: buttonColor,
                      )
                    });
                StaticValue.map.state.addMarker();
              }
            },
            elevation: 2,
            increaseHeightBy: 20,
            increaseWidthBy: 200,
            child: new Text(
              confirmButtonText,
              style: new TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w800,
              ),
            ),
            gradient: LinearGradient(
              colors: <Color>[
                buttonColor,
                buttonColor,
              ],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
        )
      ],
    );
  }
}
