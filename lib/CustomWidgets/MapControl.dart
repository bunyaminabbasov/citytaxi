import 'dart:async';

import 'package:citytaxi/Static/SharedPreferences.dart';
import 'package:citytaxi/Static/StaticValue.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_svg/svg.dart';
import 'package:flutter/material.dart';
import 'dart:math' as Math;
import 'package:location/location.dart';
import 'package:animator/animator.dart';

class MapControl extends StatefulWidget {
  _MapControlState state;
  @override
  State<StatefulWidget> createState() {
    state = new _MapControlState();
    return state;
  }
}

var location = new Location();

class _MapControlState extends State<MapControl> with TickerProviderStateMixin {
  void animatedMapMove(LatLng destLocation, double destZoom, int dur) {
    final _latTween = new Tween<double>(
        begin: mapController.center.latitude, end: destLocation.latitude);
    final _lngTween = new Tween<double>(
        begin: mapController.center.longitude, end: destLocation.longitude);
    final _zoomTween =
        new Tween<double>(begin: mapController.zoom, end: destZoom);
    AnimationController controller =
        AnimationController(duration: Duration(milliseconds: dur), vsync: this);
    Animation<double> animation =
        CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn);
    controller.addListener(() {
      mapController.move(
          LatLng(_latTween.evaluate(animation), _lngTween.evaluate(animation)),
          _zoomTween.evaluate(animation));
    });
    animation.addStatusListener((status) {
      print("$status");
      if (status == AnimationStatus.completed) {
        controller.dispose();
      } else if (status == AnimationStatus.dismissed) {
        controller.dispose();
      }
    });
    controller.forward();
  }

  void moveCarMarker(
      LatLng startLocation, LatLng destLocation, double destZoom, int dur) {
    final _latTween = new Tween<double>(
        begin: startLocation.latitude, end: destLocation.latitude);
    final _lngTween = new Tween<double>(
        begin: startLocation.longitude, end: destLocation.longitude);
    final _zoomTween =
        new Tween<double>(begin: mapController.zoom, end: destZoom);
    AnimationController controller =
        AnimationController(duration: Duration(milliseconds: dur), vsync: this);
    Animation<double> animation =
        CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn);
    controller.addListener(() {
      setState(() {
        markers[markers.length - 1].point = LatLng(
            _latTween.evaluate(animation), _lngTween.evaluate(animation));
        _zoomTween.evaluate(animation);
      });
    });
    animation.addStatusListener((status) {
      print("$status");
      if (status == AnimationStatus.completed) {
        controller.dispose();
      } else if (status == AnimationStatus.dismissed) {
        controller.dispose();
      }
    });
    controller.forward();
  }

  final Widget markdestSvg =
      new SvgPicture.asset("assets/markdest.svg", semanticsLabel: 'Markdest');
  final Widget markonSvg = new SvgPicture.asset(
    "assets/markon.svg",
    semanticsLabel: 'Markon',
    color: Color.fromRGBO(66, 100, 251, 1),
  );
  LatLng pickup_location;
  LatLng destination_location;
  List<Marker> markers = new List<Marker>();
  var points = <LatLng>[];
  var polylines = <Polyline>[];
  List<dynamic> coords;
  int mapstate = 0;
  var tempPoint;
  double carDirection = 0;
  moveMyLocation() {
    animatedMapMove(myLocation, 16, 1500);
  }

  addCar(double lat, double long) {
    setState(
      () {
        print("car added");
        markers.add(
          new Marker(
            point: new LatLng(lat, long),
            builder: (ctx) => new Container(
                  child: Transform.rotate(
                    angle: carDirection,
                    child: Image.asset(
                      'assets/supercar.png',
                      width: 30,
                      height: 30,
                    ),
                  ),
                  height: 80,
                  width: 80,
                ),
          ),
        );
      },
    );
  }

  double calculateZoomTwoPoints(LatLng p1, LatLng p2) {
    var GLOBE_WIDTH = 512; // a constant in Google's map projection
    var west = p1.longitude;
    var east = p2.longitude;
    var angle = east - west;
    if (angle < 0) {
      angle = west - east;
    }
    return (Math.log(MediaQuery.of(StaticValue.con).size.width *
                360 /
                angle /
                GLOBE_WIDTH) /
            Math.ln2)
        .round()
        .toDouble();
  }

  moveCar(double lat, double long, double direction) {
    print("moving car");
    setState(() {
      carDirection = direction;
    });
    print("---------------------------");
    print(lat);
    print(long);
    print("---------------------------");
    // mapController.fitBounds(
    //     LatLngBounds(
    //       pickup_location,
    //     ),
    //     options: new FitBoundsOptions(
    //       padding: Point(15.0, 15.0),
    //       zoom: mapController.zoom-2
    //     ));

    moveCarMarker(markers[markers.length - 1].point, new LatLng(lat, long),
        mapController.zoom, 500);
    print(
        "Zoom: ${calculateZoomTwoPoints(markers[markers.length - 1].point, pickup_location)}");
  }

  animatedCarPointFitBounds() {
    animatedMapMove(
        LatLng(
            (markers[markers.length - 1].point.latitude +
                    pickup_location.latitude) /
                2,
            (markers[markers.length - 1].point.longitude +
                    pickup_location.longitude) /
                2),
        calculateZoomTwoPoints(
                markers[markers.length - 1].point, pickup_location) -
            1,
        1500);
  }

  clear() {
    try {
      points.clear();
      polylines.clear();
      markers.clear();
      mapstate = 0;
      mainMarkerColor = Color.fromRGBO(255, 128, 0, 1);
      setState(() {
        pointerIgnored = false;
      });
      coords.clear();
    } catch (e) {}
  }

  removeAll() {
    points.clear();
    polylines.clear();
    markers.clear();
    mapstate = 0;
    mainMarkerColor = Color.fromRGBO(255, 128, 0, 1);
    setState(() {
      pointerIgnored = false;
    });
    coords.clear();
    animatedMapMove(pickup_location, 16, 1500);
    markers.add(new Marker(
        height: 50,
        width: 50,
        point: pickup_location,
        builder: (ctx) => Column(
              children: <Widget>[
                Animator(
                    tween: Tween<double>(begin: 1.1, end: 1.3),
                    curve: Curves.elasticOut,
                    duration: Duration(seconds: 2),
                    cycles: 0,
                    builder: (anim) => Transform.scale(
                          scale: anim.value,
                          child: Container(
                            child: Container(
                              margin: EdgeInsets.all(5),
                              width: 13,
                              height: 13,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                shape: BoxShape.circle,
                              ),
                            ),
                            width: 25,
                            height: 23,
                            decoration: BoxDecoration(
                              color: mainMarkerColor,
                              shape: BoxShape.circle,
                            ),
                          ),
                        )),
                Container(
                  height: 22,
                  width: 2.5,
                  decoration: BoxDecoration(
                    color: mainMarkerColor,
                  ),
                )
              ],
            )));
  }

  changeCenter(String coordinates) {
    coordinates = coordinates.replaceAll("{", "").replaceAll("}", "");
    var coo = coordinates.split(",");
    print("-----------------------------------------");
    coo.forEach((e) => {print(e)});
    print("-----------------------------------------");
    center = new LatLng(double.parse(coo[0]), double.parse(coo[1]));
    setState(() {
      mapController.move(center, 16);
      //markers[0].point = center;
    });
  }

  changeMarker(String direction, String coordinates) {
    coordinates = coordinates.replaceAll("{", "").replaceAll("}", "");
    var coo = coordinates.split(",");
    var latLng = new LatLng(double.parse(coo[0]), double.parse(coo[1]));
    if (direction == "pickup") {
      setState(() {
        markers[0].point = latLng;
        pickup_location = latLng;
      });
      makeRoute();
    } else {
      setState(() {
        markers[1].point = latLng;
        destination_location = latLng;
      });
      makeRoute();
    }
  }

  void addMarker() {
    mapstate++;
    if (mapstate == 2) {
      markers.add(new Marker(
        point: markers[0].point,
        builder: (ctx) => new Container(
              child: Column(
                children: <Widget>[
                  Container(
                    child: Container(
                      margin: EdgeInsets.all(5),
                      width: 13,
                      height: 13,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        shape: BoxShape.circle,
                      ),
                    ),
                    width: 23,
                    height: 23,
                    decoration: BoxDecoration(
                      color: Color.fromRGBO(66, 100, 251, 1),
                      shape: BoxShape.circle,
                    ),
                  ),
                  Container(
                    height: 15,
                    width: 2,
                    decoration: BoxDecoration(
                      color: Color.fromRGBO(66, 100, 251, 1),
                    ),
                  )
                ],
              ),
            ),
        height: 50,
        width: 50,
      ));
      setState(() => {
            pickup_location = markers[1].point,
            destination_location = markers[2].point,
            print(pickup_location),
            print(destination_location),
            makeRoute()
          });
    } else if (mapstate == 1) {
      markers.add(new Marker(
          point: markers[0].point,
          builder: (ctx) => Column(
                children: <Widget>[
                  Container(
                    child: Container(
                      margin: EdgeInsets.all(5),
                      width: 13,
                      height: 13,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        shape: BoxShape.circle,
                      ),
                    ),
                    width: 23,
                    height: 23,
                    decoration: BoxDecoration(
                      color: Color.fromRGBO(255, 128, 0, 1),
                      shape: BoxShape.circle,
                    ),
                  ),
                  Container(
                    height: 15,
                    width: 2,
                    decoration: BoxDecoration(
                      color: Color.fromRGBO(255, 128, 0, 1),
                    ),
                  )
                ],
              ),
          height: 50,
          width: 50));
      setState(() {
        mainMarkerColor = Color.fromRGBO(66, 100, 251, 1);
      });
      animatedMapMove(addDistanceToPoint(markers[0].point, 50), 16, 300);
    }
  }

  LatLng addDistanceToPoint(LatLng point, double meters) {
    double coef = meters * 0.0000089;
    double newlong = point.longitude + coef / Math.cos(point.latitude * 0.018);
    return LatLng(point.latitude - coef, newlong);
  }

  addBothMarkers() {
    pickup_location = LatLng(
        StaticValue.activeOrder.startLat, StaticValue.activeOrder.startLong);
    destination_location =
        LatLng(StaticValue.activeOrder.endLat, StaticValue.activeOrder.endLong);
    setState(() {
      markers.add(new Marker(
          point: LatLng(StaticValue.activeOrder.startLat,
              StaticValue.activeOrder.startLong),
          builder: (ctx) => Column(
                children: <Widget>[
                  Container(
                    child: Container(
                      margin: EdgeInsets.all(5),
                      width: 13,
                      height: 13,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        shape: BoxShape.circle,
                      ),
                    ),
                    width: 23,
                    height: 23,
                    decoration: BoxDecoration(
                      color: Color.fromRGBO(255, 128, 0, 1),
                      shape: BoxShape.circle,
                    ),
                  ),
                  Container(
                    height: 15,
                    width: 2,
                    decoration: BoxDecoration(
                      color: Color.fromRGBO(255, 128, 0, 1),
                    ),
                  )
                ],
              ),
          height: 50,
          width: 50));

      markers.add(new Marker(
        point:LatLng(StaticValue.activeOrder.endLat,StaticValue.activeOrder.endLong),
        builder: (ctx) => new Container(
              child: Column(
                children: <Widget>[
                  Container(
                    child: Container(
                      margin: EdgeInsets.all(5),
                      width: 13,
                      height: 13,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        shape: BoxShape.circle,
                      ),
                    ),
                    width: 23,
                    height: 23,
                    decoration: BoxDecoration(
                      color: Color.fromRGBO(66, 100, 251, 1),
                      shape: BoxShape.circle,
                    ),
                  ),
                  Container(
                    height: 15,
                    width: 2,
                    decoration: BoxDecoration(
                      color: Color.fromRGBO(66, 100, 251, 1),
                    ),
                  )
                ],
              ),
            ),
        height: 50,
        width: 50,
      ));
    });
    addCar(StaticValue.orderinfo.carLat, StaticValue.orderinfo.carLong);
    mapstate = 2;
    makeRouteee();
  //  StaticValue.map.state.animatedCarPointFitBounds();
    StaticValue.fitBoundsTimer = new Timer.periodic(
          Duration(milliseconds: 7000),
          (timer) async =>
              {await StaticValue.map.state.animatedCarPointFitBounds()});
      StaticValue.orderstatusTimer = new Timer.periodic(
          Duration(milliseconds: 1500), (timer) async => {await moveCarrrrr()});
  }
  moveCarrrrr() async {
    String result = await StaticValue.orderStatus(
        StaticValue.orderId, StaticValue.hasher(StaticValue.orderId));
    if (result == "ok") {
      if (StaticValue.orderstatus.status == "0") {
        await StaticValue.cancel(StaticValue.phone, StaticValue.orderId,
            StaticValue.hasher(StaticValue.orderId + StaticValue.phone));
        await StaticValue.deleteOrderInfos();
        StaticValue.fitBoundsTimer.cancel();
        StaticValue.orderstatusTimer.cancel();
        StaticValue.map.state.removeAll();
        StaticValue.mapPageState.removeAll();
      }
      StaticValue.map.state.moveCar(
          StaticValue.orderstatus.currentLat,
          StaticValue.orderstatus.currentLong,
          StaticValue.orderstatus.currentDirection);
    }
  }
  void makeRoute() {
    if (mapstate == 2) {
      setState(() {
        markers.removeAt(0);
        var bounds = new LatLngBounds();
        bounds.extend(pickup_location);
        bounds.extend(destination_location);
        try {
          mapController.fitBounds(bounds,
              options: new FitBoundsOptions(
                padding: Point(15.0, 15.0),
              ));
          animatedMapMove(mapController.center, mapController.zoom - 1.5, 1500);
        } catch (e) {}
      });
    }
    setState(() {
      points.clear();
      points = null;
      points = new List<LatLng>();
      polylines.clear();
      polylines = null;
      polylines = new List<Polyline>();
    });
    setState(() => {
          http
              .get(
                  "https://api.mapbox.com/directions/v5/mapbox/driving/${pickup_location.longitude},${pickup_location.latitude};${destination_location.longitude},${destination_location.latitude}?geometries=geojson&access_token=pk.eyJ1IjoiYm5rIiwiYSI6ImNqdGt2Y2QxbDFhemI0YW82dnQ1OTc0cWYifQ.sJOF7T9dMYLevWoAR_bdkA")
              .then((val) => {
                    setState(() {
                      coords = json.decode(val.body)['routes'][0]['geometry']
                          ['coordinates'];

                      for (var i = 0; i < coords.length; i++) {
                        tempPoint = new List<LatLng>();
                        tempPoint.add(LatLng(
                            coords[i][1] as double, coords[i][0] as double));
                        if (i + 1 < coords.length) {
                          tempPoint.add(LatLng(coords[i + 1][1] as double,
                              coords[i + 1][0] as double));
                        }
                        setState(() {
                          polylines.add(
                            new Polyline(
                              points: tempPoint,
                              strokeWidth: 5.0,
                              color: Color.fromRGBO(255, 159, 122, 1),
                            ),
                          );
                        });
                      }
                    }),
                  }),
        });
  }

  void makeRouteee() {
    setState(() => {
          http
              .get(
                  "https://api.mapbox.com/directions/v5/mapbox/driving/${pickup_location.longitude},${pickup_location.latitude};${destination_location.longitude},${destination_location.latitude}?geometries=geojson&access_token=pk.eyJ1IjoiYm5rIiwiYSI6ImNqdGt2Y2QxbDFhemI0YW82dnQ1OTc0cWYifQ.sJOF7T9dMYLevWoAR_bdkA")
              .then((val) => {
                    setState(() {
                      coords = json.decode(val.body)['routes'][0]['geometry']
                          ['coordinates'];

                      for (var i = 0; i < coords.length; i++) {
                        tempPoint = new List<LatLng>();
                        tempPoint.add(LatLng(
                            coords[i][1] as double, coords[i][0] as double));
                        if (i + 1 < coords.length) {
                          tempPoint.add(LatLng(coords[i + 1][1] as double,
                              coords[i + 1][0] as double));
                        }
                        setState(() {
                          polylines.add(
                            new Polyline(
                              points: tempPoint,
                              strokeWidth: 5.0,
                              color: Color.fromRGBO(255, 159, 122, 1),
                            ),
                          );
                        });
                      }
                    }),
                  }),
        });
  }

  MapController mapController;
  moveMarker(MapPosition pos) async {
    if (mapstate < 2) {
      if (markers.length > 0) {
        markers[0].point = pos.center;
      }
    }
  }

  var mainMarkerColor = Color.fromRGBO(255, 128, 0, 1);
  var location = new Location();
  double zoom = 0;
  LatLng myLocation;
  @override
  void initState() {
    mapController = new MapController();
    location.onLocationChanged().listen((LocationData currentLocation) {
      myLocation =
          new LatLng(currentLocation.latitude, currentLocation.longitude);
    });
    if (StaticValue.orderId != null) {
      destination_location = StaticValue.destloc;
      pickup_location = StaticValue.picloc;
      center = LatLng(
          (destination_location.latitude + pickup_location.latitude) / 2,
          (destination_location.longitude + pickup_location.longitude) / 2);
      zoom = calculateZoomTwoPoints(destination_location, pickup_location);
      addBothMarkers();
    } else {
      center = new LatLng(40.4093, 49.8671);
      markers.add(new Marker(
          height: 50,
          width: 50,
          point: center,
          builder: (ctx) => Column(
                children: <Widget>[
                  Animator(
                      tween: Tween<double>(begin: 1.1, end: 1.3),
                      curve: Curves.elasticOut,
                      duration: Duration(seconds: 2),
                      cycles: 0,
                      builder: (anim) => Transform.scale(
                            scale: anim.value,
                            child: Container(
                              child: Container(
                                margin: EdgeInsets.all(5),
                                width: 13,
                                height: 13,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  shape: BoxShape.circle,
                                ),
                              ),
                              width: 25,
                              height: 23,
                              decoration: BoxDecoration(
                                color: mainMarkerColor,
                                shape: BoxShape.circle,
                              ),
                            ),
                          )),
                  Container(
                    height: 22,
                    width: 2.5,
                    decoration: BoxDecoration(
                      color: mainMarkerColor,
                    ),
                  )
                ],
              )));
      startMap();
    }
    super.initState();
  }

  bool pointerIgnored = false;
  LatLng center;
  @override
  Widget build(BuildContext context) {
    return Listener(
        onPointerUp: (e) async {
          if (mapstate < 2) {
            print("mapstate: ${mapstate}");
            try {
              await StaticValue.homeState.changeCurrentLocStr();
            } catch (e) {}
          }
        },
        child: FlutterMap(
          mapController: mapController,
          options: new MapOptions(
              onTap: (e) {},
              onPositionChanged: (MapPosition pos) async =>
                  {await moveMarker(pos), center = mapController.center},
              center: LatLng(40.4093, 49.8671),
              zoom: zoom == 0 ? 16 : zoom,
              maxZoom: 18,
              minZoom: 5),
          layers: [
            new TileLayerOptions(
              subdomains: ['A', 'B', 'C', 'D'],
              maxZoom: 20.0,
              urlTemplate:
                  "https://api.mapbox.com/styles/v1/bnk/cjtkvdnii105d1fryuq8lzak9/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoiYm5rIiwiYSI6ImNqdGt2Y2QxbDFhemI0YW82dnQ1OTc0cWYifQ.sJOF7T9dMYLevWoAR_bdkA#9.3/42.354918/-71.016156/0",
              additionalOptions: {
                'id': '3d-buildings',
              },
            ),
            new PolylineLayerOptions(polylines: polylines),
            new MarkerLayerOptions(
              markers: markers,
            ),
          ],
        ));
  }

  startMap() async {
    try {
      var currentLocation = await location.getLocation();
      center = new LatLng(currentLocation.latitude, currentLocation.longitude);
      StaticValue.homeState.changeCurrentLocStr();
    } on Exception catch (e) {
      center = new LatLng(40.4093, 49.8671);
    } finally {
      setState(() {
        animatedMapMove(center, 16, 1500);
      });
    }
  }
}
