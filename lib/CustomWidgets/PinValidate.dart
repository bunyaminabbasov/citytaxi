import 'dart:io';
import 'package:citytaxi/Static/SharedPreferences.dart';
import 'package:citytaxi/Static/StaticValue.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pin_view/pin_view.dart';
import 'package:gradient_widgets/gradient_widgets.dart';
import 'package:citytaxi/Pages/LoginPage.dart';
import 'package:citytaxi/Pages/MapPage.dart';

class PinValidate extends StatefulWidget {
  final LoginPageState parent;
  final String phone;
  PinValidate(this.parent, this.phone);
  @override
  State<StatefulWidget> createState() => _PinValidateState(parent, phone);
}

class _PinValidateState extends State<PinValidate> {
  LoginPageState parent;
  String phone;
  String code;
  _PinValidateState(this.parent, this.phone);
  SmsListener smsListener = SmsListener(
    from: 'TAXION-9911',
    formatBody: (String body) {
      String codeRaw = body.split(": ")[1];
      return codeRaw;
    },
  );
  final Widget logoSvg =
      new SvgPicture.asset("assets/logo.svg", semanticsLabel: 'Logo');

  Future<void> checkCode() async {
    if (code.length == 4) {
      setState(() {
       StaticValue.amazingLoading(context);
      });
      var result = await StaticValue.verifyCode(
          this.phone, this.code, StaticValue.hasher(this.phone));
      setState(() {
       Navigator.of(context).pop();
      });
      if (result == "ok") {
        await SharedPref.saveV("phone", StaticValue.phone);
        await SharedPref.saveV(
            "iscoop", StaticValue.isCoop ? "coop" : "notcoop");
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => MapPage()),
        );
      } else if (result == "notok") {
        await showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: RichText(
                text: TextSpan(
                  children: <TextSpan>[
                    TextSpan(
                      text: 'CityTaxi Corporate ',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
              content: Text("Kod yanlışdır."),
              actions: <Widget>[
                FlatButton(
                  child: Text(
                    "Bəli",
                    style: TextStyle(
                      color: Colors.red,
                    ),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      } else {
        await showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: RichText(
                text: TextSpan(
                  children: <TextSpan>[
                    TextSpan(
                      text: 'CityTaxi Corporate ',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
              content: Text("Xəta baş verdi."),
              actions: <Widget>[
                FlatButton(
                  child: Text(
                    "Bəli",
                    style: TextStyle(
                      color: Colors.red,
                    ),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: parent.queryData.size.height * 0.04),
            child: logoSvg,
          ),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: parent.queryData.size.width * 0.08,
              vertical: parent.queryData.size.width * 0.08,
            ),
            child: Material(
              child: PinView(
                submit: (String pin) {
                  code = pin;
                },
                context: context,
                autoFocusFirstField: false,
                sms: smsListener,
                dashStyle: TextStyle(
                    color: Colors.tealAccent, fontWeight: FontWeight.bold),
                count: 4,
              ),
            ),
          ),
          Container(
            height: parent.queryData.size.height * 0.03,
          ),
          Text(
            "SMS sent to",
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w600,
            ),
          ),
          Container(
            height: parent.queryData.size.height * 0.01,
          ),
          Text(
            this.phone,
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w600,
            ),
          ),
          Container(
            height: parent.queryData.size.height * 0.03,
          ),
          new GradientButton(
            callback: () async {
              await checkCode();
            },
            elevation: 2,
            increaseHeightBy: 0.4,
            increaseWidthBy: 110,
            child: new Text(
              "Növbəti",
              style: new TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w800,
              ),
            ),
            gradient: LinearGradient(
              colors: <Color>[
                Color.fromRGBO(255, 128, 0, 1),
                Color.fromRGBO(255, 128, 0, 1),
              ],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          )
        ],
      
    );
  }
}
