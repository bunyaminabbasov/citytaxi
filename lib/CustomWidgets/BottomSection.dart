import 'dart:async';
import 'package:animator/animator.dart';
import 'package:citytaxi/Static/StaticValue.dart';
import 'package:flutter/material.dart';
import 'package:citytaxi/Static/SharedPreferences.dart' as shared;
import 'package:url_launcher/url_launcher.dart';

class BottomSection extends StatefulWidget {
  final int choosen;
  BottomSection(this.choosen);
  @override
  State<StatefulWidget> createState() {
    return BottomSectionState(this.choosen);
  }
}

class BottomSectionState extends State<BottomSection> {
  Color color;
  final int choosen;
  BottomSectionState(this.choosen);
  String driverPhone;
  @override
  void initState() {
    color = Colors.white;
    if (this.choosen == 1) {
      childWidget = new TripReguest(this);
    } else {
      childWidget = new DriverInfoWidget(this);
    }

    super.initState();
  }

  MediaQueryData queryData;
  Widget childWidget;
  void changeChild(int i) {
    if (i == 1) {
      setState(() =>
          {childWidget = new DriverInfoWidget(this), color = Colors.white});
    }
  }

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);
    return new GestureDetector(
      child: Container(
        padding: EdgeInsets.only(top: queryData.size.height * 0.03),
        margin: EdgeInsets.only(
          top: queryData.size.height * 0.666,
          left: queryData.size.width * 0.07,
          right: queryData.size.width * 0.07,
        ),
        decoration: BoxDecoration(
          boxShadow: <BoxShadow>[
            new BoxShadow(
              color: Colors.transparent,
              offset: new Offset(0.2, 0.2),
              blurRadius: 5.0,
            ),
          ],
          borderRadius: BorderRadius.only(
              topLeft: new Radius.circular(10),
              topRight: new Radius.circular(10)),
          color: color,
        ),
        child: childWidget,
      ),
    );
  }
}

class DriverInfoWidget extends StatefulWidget {
  BottomSectionState parent;
  DriverInfoWidget(this.parent);
  @override
  State<StatefulWidget> createState() {
    return _DriverInfoWidgetState(parent);
  }
}

class _DriverInfoWidgetState extends State<DriverInfoWidget> {
  MediaQueryData queryData;
  BottomSectionState parent;
  _DriverInfoWidgetState(this.parent);
  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);
    return new InkWell(
      onTap: (() => {parent.changeChild(2)}),
      child: Column(
        children: <Widget>[
          new Center(
            child: new Text(
              StaticValue.orderinfo.driverFullname,
              style: new TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
            ),
          ),
          new Container(
            margin: EdgeInsets.only(top: queryData.size.height * 0.01),
            child: new Center(
              child: new Text(
                StaticValue.orderinfo.carInfo,
                style: new TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w300,
                    color: Colors.grey[510]),
              ),
            ),
          ),
          new Container(
            decoration: new BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(5),
                ),
                border: Border.all(color: Colors.grey[500])),
            margin: EdgeInsets.only(
                top: queryData.size.height * 0.015,
                left: queryData.size.width * 0.285,
                right: queryData.size.width * 0.285),
            child: new Center(
              child: Container(
                padding: EdgeInsets.symmetric(
                    vertical: queryData.size.height * 0.005),
                child: new Text(
                  StaticValue.orderinfo.carNumber,
                  style: new TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Colors.grey[510]),
                ),
              ),
            ),
          ),
         Container(
           margin: EdgeInsets.only(top:queryData.size.height * 0.028),
           child:  CallWidget(parent),
         )
        ],
        
      ),
    );
  }
}

class CallWidget extends StatefulWidget {
  BottomSectionState parent;
  CallWidget(this.parent);
  @override
  State<StatefulWidget> createState() {
    return _CallWidgetSate(parent);
  }
}

class _CallWidgetSate extends State<CallWidget> {
  BottomSectionState parent;
  _CallWidgetSate(this.parent);
  MediaQueryData queryData;
  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);
    return Row(
      children: <Widget>[
        Center(
            child: Container(
          margin: EdgeInsets.only(left: queryData.size.width * 0.02),
          height: queryData.size.height * 0.07,
          width: queryData.size.width * 0.4,
          child: FlatButton.icon(
            onPressed: () =>
                {launch("tel:${StaticValue.orderinfo.driverPhone}")},
            color: Colors.green,
            icon: Icon(
              Icons.call,
              color: Colors.white,
              size: 20,
            ),
            label: Text(
              "Sürücüyə Zəng",
              style: TextStyle(color: Colors.white, fontSize: 13),
            ),
          ),
        )),
        Center(
            child: Container(
          margin: EdgeInsets.only(
              right: queryData.size.width * 0.01,
              left: queryData.size.width * 0.02),
          height: queryData.size.height * 0.07,
          width: queryData.size.width * 0.4,
          child: OutlineButton(
            borderSide: BorderSide(color: Color.fromRGBO(255, 71, 1, 1)),
            onPressed: () async => {
                  await showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: RichText(
                          text: TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                text: 'CityTaxi Corporate ',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ],
                          ),
                        ),
                        content:
                            Text("İmtina etmək istədiyinizdən əminsinizmi?"),
                        actions: <Widget>[
                          FlatButton(
                            child: Text(
                              "Xeyr",
                              style: TextStyle(
                                color: Colors.black,
                              ),
                            ),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                          FlatButton(
                            child: Text(
                              "Bəli",
                              style: TextStyle(
                                color: Colors.red,
                              ),
                            ),
                            onPressed: () async {
                              var response = await StaticValue.cancel(
                                  StaticValue.phone,
                                  StaticValue.orderId,
                                  StaticValue.hasher(
                                      StaticValue.orderId + StaticValue.phone));
                              if (response == "ok") {
                                StaticValue.isOrdered = false;
                                StaticValue.fitBoundsTimer.cancel();
                                StaticValue.orderstatusTimer.cancel();
                                StaticValue.map.state.removeAll();
                                StaticValue.mapPageState.removeAll();
                                Navigator.of(context).pop();
                              } else {
                                await showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: RichText(
                                        text: TextSpan(
                                          children: <TextSpan>[
                                            TextSpan(
                                              text: 'CityTaxi Corporate ',
                                              style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      content: Text("Xəta baş verdi."),
                                      actions: <Widget>[
                                        FlatButton(
                                          child: Text(
                                            "Bəli",
                                            style: TextStyle(
                                              color: Colors.red,
                                            ),
                                          ),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                      ],
                                    );
                                  },
                                );
                              }
                            },
                          ),
                        ],
                      );
                    },
                  ),

                  //parent.changeChild(1)
                },
            color: Colors.green,
            child: Text(
              "İmtina et",
              style:
                  TextStyle(color: Color.fromRGBO(255, 71, 1, 1), fontSize: 13),
            ),
          ),
        ))
      ],
    );
  }
}

class TripReguest extends StatefulWidget {
  BottomSectionState parent;
  TripReguest(this.parent);
  @override
  State<StatefulWidget> createState() {
    return _TripReguestSate(parent);
  }
}

String response;
var orderResponse;

//----------------------TRIPPPPPPPPPPP----------------
class _TripReguestSate extends State<TripReguest> {
  @override
  onCancelClick() async {
    Navigator.of(context).pop();
    if (StaticValue.isOrdered) {
      if (StaticValue.orderId != null) {
        var response = await StaticValue.cancel(
            StaticValue.phone,
            StaticValue.orderId,
            StaticValue.hasher(StaticValue.orderId + StaticValue.phone));
        if (response == "ok") {
          StaticValue.ordercheckTimer.cancel();
          StaticValue.isOrdered = false;
          StaticValue.orderId = null;
          await shared.SharedPref.delete("orderid");
        } else {
          await showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: RichText(
                  text: TextSpan(
                    children: <TextSpan>[
                      TextSpan(
                        text: 'CityTaxi Corporate ',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                ),
                content: Text("Xəta baş verdi."),
                actions: <Widget>[
                  FlatButton(
                    child: Text(
                      "Bəli",
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            },
          );
          return;
        }
      } else {
        return;
      }
    }
    StaticValue.map.state.removeAll();
    StaticValue.mapPageState.removeAll();
  }

  moveCar() async {
    String result = await StaticValue.orderStatus(
        StaticValue.orderId, StaticValue.hasher(StaticValue.orderId));
    if (result == "ok") {
      if (StaticValue.orderstatus.status == "0") {
        await StaticValue.cancel(StaticValue.phone, StaticValue.orderId,
            StaticValue.hasher(StaticValue.orderId + StaticValue.phone));
        await shared.SharedPref.delete("orderid");
        StaticValue.fitBoundsTimer.cancel();
        StaticValue.orderstatusTimer.cancel();
        StaticValue.map.state.removeAll();
        StaticValue.mapPageState.removeAll();
      }
      StaticValue.map.state.moveCar(
          StaticValue.orderstatus.currentLat,
          StaticValue.orderstatus.currentLong,
          StaticValue.orderstatus.currentDirection);
    }
  }

  checkorder(Timer timerS) async {
    orderResponse = await StaticValue.orderInfo(
        StaticValue.orderId, StaticValue.hasher(StaticValue.orderId));
    if (orderResponse == "ok") {
      await StaticValue.saveOrderInfos();
      timerS.cancel();
      StaticValue.map.state
          .addCar(StaticValue.orderinfo.carLat, StaticValue.orderinfo.carLong);
      // animatedCarPointFitBounds
      StaticValue.fitBoundsTimer = new Timer.periodic(
          Duration(milliseconds: 7000),
          (timer) async =>
              {await StaticValue.map.state.animatedCarPointFitBounds()});
      StaticValue.orderstatusTimer = new Timer.periodic(
          Duration(milliseconds: 1500), (timer) async => {await moveCar()});
      parent.driverPhone = StaticValue.orderinfo.driverPhone;
      parent.changeChild(1);
      print(orderResponse);
    }
  }

  startOrder() async {
    if (acceptEnabled) {
      setState(() => {
            wg = Container(
              margin: EdgeInsets.only(top: 5, bottom: 12),
              alignment: Alignment.bottomCenter,
              child: Animator(
                tween:
                    Tween<Offset>(begin: Offset(-100, 0), end: Offset(100, 0)),
                duration: Duration(milliseconds: 400),
                cycles: 0,
                builder: (anim) => Transform.translate(
                      offset: anim.value,
                      child: Container(
                          color: Color.fromRGBO(255, 71, 1, 1),
                          height: 3,
                          width: 50),
                    ),
              ),
            ),
            acceptEnabled = false
          });
      response = await StaticValue.order(
          StaticValue.pickupStreet,
          StaticValue.destinationStreet,
          StaticValue.phoneParser(StaticValue.phone.replaceAll("+994", "")),
          StaticValue.hasher(StaticValue.phoneParser(
              StaticValue.phone.replaceAll("+994", ""))),
          StaticValue.map.state.pickup_location.latitude,
          StaticValue.map.state.pickup_location.longitude,
          StaticValue.map.state.destination_location.latitude,
          StaticValue.map.state.destination_location.longitude,
          selfPressed ? 0 : 1);
      if (response == "ok") {
        await shared.SharedPref.saveV("orderid", StaticValue.orderId);
        StaticValue.ordercheckTimer = new Timer.periodic(
            Duration(seconds: 5), (timer) async => {await checkorder(timer)});
      } else if (response == "notok" || response == "mistake") {
        await showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: RichText(
                text: TextSpan(
                  children: <TextSpan>[
                    TextSpan(
                      text: 'CityTaxi Corporate ',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
              content: Text("Xəta baş verdi."),
              actions: <Widget>[
                FlatButton(
                  child: Text(
                    "Bəli",
                    style: TextStyle(
                      color: Colors.red,
                    ),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
        setState(() {
          wg = Divider(
            height: 20,
          );
          acceptEnabled = true;
        });
        StaticValue.isOrdered = false;
      } else {
        StaticValue.isOrdered = false;

        setState(() {
          wg = Divider(
            height: 20,
          );
          acceptEnabled = true;
        });
      }
    }
  }

  BottomSectionState parent;
  _TripReguestSate(this.parent);
  MediaQueryData queryData;
  bool acceptEnabled = true;

  Widget wg = Divider(
    height: 20,
  );
  var selfActive = Image.asset("assets/self_payment_active.png");
  var selfDeActive = Image.asset("assets/self_payment_deactive.png");
  var companyActive = Image.asset("assets/company_payment_active.png");
  var companyDeActive = Image.asset("assets/company_payment_deactive.png");
  bool selfPressed = !StaticValue.isCoop;
  bool companyPressed = StaticValue.isCoop;

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);
    StaticValue.orderType = selfPressed ? 0 : 1;

    return Column(
      children: <Widget>[
        Container(
          child: new Text("${StaticValue.price} ₼",
              style: new TextStyle(
                  color: Colors.green,
                  fontWeight: FontWeight.w600,
                  fontSize: 18)),
        ),
        wg,
        Row(
          children: <Widget>[],
        ),
        Row(
          children: <Widget>[
            Column(children: [
              Column(
                children: <Widget>[
                  Center(
                      child: Container(
                          margin: EdgeInsets.only(bottom: 4),
                          height: 60,
                          width: 60,
                          decoration: BoxDecoration(shape: BoxShape.circle),
                          child: InkWell(
                            onTap: () {
                              setState(()  {
                                if (!StaticValue.isOrdered &&
                                    StaticValue.isCoop) {
                                  selfPressed = false;
                                  companyPressed = true;
                                  StaticValue.orderType = 1;
                                } else if (!StaticValue.isCoop) {
                                   showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        title: RichText(
                                          text: TextSpan(
                                            children: <TextSpan>[
                                              TextSpan(
                                                text: 'CityTaxi Corporate ',
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        content: Text(
                                            "Siz korporativ muştəri deyilsiniz."),
                                        actions: <Widget>[
                                          FlatButton(
                                            child: Text(
                                              "Bəli",
                                              style: TextStyle(
                                                color: Colors.black,
                                              ),
                                            ),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      );
                                    },
                                  );
                                }
                              });
                            },
                            child: companyPressed
                                ? companyActive
                                : companyDeActive,
                          ))),
                  Center(
                    child: Text("Şirkət Hesabı"),
                  )
                ],
              ),
              Center(
                  child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(
                        left: queryData.size.width * 0.02, top: 4),
                    height: queryData.size.height * 0.07,
                    width: queryData.size.width * 0.4,
                    child: FlatButton(
                      onPressed: () async {
                        if (!StaticValue.isOrdered) {
                          StaticValue.isOrdered = true;
                          startOrder();
                        }
                      },
                      color: Colors.green,
                      child: Text(
                        "Təsdiqlə",
                        style: TextStyle(color: Colors.white, fontSize: 13),
                      ),
                      disabledColor: Colors.green[600],
                    ),
                  )
                ],
              )),
            ]),
            Column(children: [
              Column(
                children: <Widget>[
                  Center(
                      child: Container(
                          margin: EdgeInsets.only(bottom: 4),
                          height: 60,
                          width: 60,
                          decoration: BoxDecoration(shape: BoxShape.circle),
                          child: InkWell(
                            onTap: () {
                              setState(()  {
                                if (!StaticValue.isOrdered &&
                                    StaticValue.isCoop) {
                                  selfPressed = true;
                                  companyPressed = false;
                                  StaticValue.orderType = 0;
                                } else if (!StaticValue.isCoop) {
                                   showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        title: RichText(
                                          text: TextSpan(
                                            children: <TextSpan>[
                                              TextSpan(
                                                text: 'CityTaxi Corporate ',
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        content: Text(
                                            "Siz korporativ muştəri deyilsiniz."),
                                        actions: <Widget>[
                                          FlatButton(
                                            child: Text(
                                              "Bəli",
                                              style: TextStyle(
                                                color: Colors.black,
                                              ),
                                            ),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      );
                                    },
                                  );
                                }
                              });
                            },
                            child: selfPressed ? selfActive : selfDeActive,
                          ))),
                  Center(
                    child: Text("Öz Hesabı"),
                  ),
                ],
              ),
              Center(
                  child: Container(
                margin: EdgeInsets.only(
                    right: queryData.size.width * 0.01,
                    left: queryData.size.width * 0.02,
                    top: 4),
                height: queryData.size.height * 0.07,
                width: queryData.size.width * 0.4,
                child: FlatButton(
                  onPressed: (() async => {
                        await showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: RichText(
                                text: TextSpan(
                                  children: <TextSpan>[
                                    TextSpan(
                                      text: 'CityTaxi Corporate ',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              content: Text(
                                  "İmtina etmək istədiyinizdən əminsinizmi?"),
                              actions: <Widget>[
                                FlatButton(
                                  child: Text(
                                    "Xeyr",
                                    style: TextStyle(
                                      color: Colors.black,
                                    ),
                                  ),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                                FlatButton(
                                  child: Text(
                                    "Bəli",
                                    style: TextStyle(
                                      color: Colors.red,
                                    ),
                                  ),
                                  onPressed: () async {
                                    await onCancelClick();
                                  },
                                ),
                              ],
                            );
                          },
                        ),
                      }),
                  color: Color.fromRGBO(255, 71, 1, 1),
                  child: Text(
                    "İmtina et",
                    style: TextStyle(color: Colors.white, fontSize: 13),
                  ),
                ),
              )),
            ])
          ],
        )
      ],
    );
  }
}
