import 'package:citytaxi/Static/StaticValue.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class SplashScreen extends StatefulWidget {
  _SplashScreenState createState() => _SplashScreenState();
}
class _SplashScreenState extends State<SplashScreen> {
  void navigationToNextPage() {
  }
  startSplashScreenTimer() async {
    var _duration = new Duration(seconds: 4);
    return new Timer(_duration, navigationToNextPage);
  }
  @override
  void initState() {

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    
    return Material(child: Stack(
      children: <Widget>[ 
        Container(
          color: Colors.white,
          child: Center(
            child: Image.asset('assets/splashscreen.png'),
          ),
        ),
        Align(
          alignment: FractionalOffset.bottomCenter,
          child: Container(
            padding: EdgeInsets.only(
              bottom: MediaQuery.of(context).size.height * 0.1,
            ),
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(
                Color.fromRGBO(255, 128, 0, 1),
              ),
            ),
          ),
        ),
      ],
    ),);
  }
}
