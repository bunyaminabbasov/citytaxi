import 'dart:async';
import 'package:citytaxi/Static/StaticValue.dart';
import 'package:flutter/material.dart';
import 'package:citytaxi/Pages/LoginPage.dart';
import 'package:citytaxi/Pages/MapPage.dart';
import 'package:citytaxi/Static/SharedPreferences.dart';
import 'package:citytaxi/CustomWidgets/SplashScreen.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';

import 'Models/Order.dart';

void main() async {
  await SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  runApp(TaxiApp());
}

class TaxiApp extends StatefulWidget {
  @override
  _TaxiAppState createState() => _TaxiAppState();
}

class _TaxiAppState extends State<TaxiApp> {
  ThemeData buildTheme() {
    final ThemeData base = ThemeData();
    return base.copyWith(
        cursorColor: Color.fromRGBO(255, 71, 1, 0),
        hintColor: Colors.grey,
        iconTheme: IconThemeData(color: Colors.black));
  }

  changeState(value) async {
    if (value != null) {
      StaticValue.phone = value;
      StaticValue.isCoop =
          await SharedPref.getV("iscoop") == "coop" ? true : false;
      var orderid = await SharedPref.getV("orderid");
      if (orderid != null) {
        print(orderid);
        StaticValue.orderId = orderid;
        String response = await StaticValue.orderStatus(
            StaticValue.orderId, StaticValue.hasher(StaticValue.orderId));
        if (response != "ok" || StaticValue.orderstatus.status == "0") {
          StaticValue.orderId = null;
          await StaticValue.deleteOrderInfos();
        } else {
          await StaticValue.getOrderInfos();
        }
      }
      setState(() {
        wg = MapPage();
      });
    } else {
      await SharedPref.clearV();
      setState(() {
        wg = LoginPage();
      });
    }
  }

  @override
  void initState() {
    handlePermissions();
    super.initState();
  }

  void handlePermissions() async {
    PermissionStatus locP = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.location);
    PermissionStatus smsP =
        await PermissionHandler().checkPermissionStatus(PermissionGroup.sms);
    if (locP == PermissionStatus.granted && smsP == PermissionStatus.granted) {
      Timer(Duration(milliseconds: 1500), () async {
        var phoneValue = await SharedPref.getV("phone");

        await changeState(phoneValue);
      });
    } else {
      await PermissionHandler().requestPermissions([
        PermissionGroup.location,
        PermissionGroup.sms
      ]).then((p) => handlePermissions());
    }
  }

  Widget wg = SplashScreen();
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: wg,
      theme: buildTheme(),
    );
  }
}
