import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:citytaxi/CustomWidgets/MapHome.dart';
import 'package:citytaxi/CustomWidgets/MapControl.dart';
import 'package:citytaxi/Models/History.dart';
import 'package:citytaxi/Models/OrderInfo.dart';
import 'package:citytaxi/Models/OrderStatus.dart';
import 'package:citytaxi/Pages/MapPage.dart';
import 'package:citytaxi/Pages/NoConnectionPage.dart';
import 'package:convert/convert.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:flutter_svg/svg.dart';
import 'package:http/http.dart' as http;
import 'package:citytaxi/Models/Order.dart';
import 'package:latlong/latlong.dart';

import 'SharedPreferences.dart';

class StaticValue {
//----------------------------CONTROLS START---------------------------------------------
  static MapControl map;
  static MapPageState mapPageState;
  static MapHomeState homeState;
  static String pickupStreet;
  static String destinationStreet;
  static String phone;
  static Widget car =
      new SvgPicture.asset("assets/car.svg", semanticsLabel: 'car');
  static Widget cardrawer =
      new SvgPicture.asset("assets/cardrawer.svg", semanticsLabel: 'cardrawer');
  static Widget logout =
      new SvgPicture.asset("assets/logout.svg", semanticsLabel: 'logout');
  static Widget profile =
      new SvgPicture.asset("assets/profile.svg", semanticsLabel: 'profile');
  static Widget home =
      new SvgPicture.asset("assets/home.svg", semanticsLabel: 'home');

//----------------------------CONTROLS END---------------------------------------------

//---------------------MEMBERS START---------------
  static Timer ordercheckTimer;
  static Timer orderstatusTimer;
  static Timer fitBoundsTimer;
  static OrderInfo orderinfo;
  static OrderStatus orderstatus;
  static Order activeOrder;
  static bool isCoop = false;
  static String price;
  static String orderId;
  static bool isOrdered = false;
  static int orderType = 0;
  static bool isConnected = true;
  // static bool haveOrderId = false;
  static var subscription;

//---------------------MEMBERS END---------------

//-----------------------------------UTILITIES START------------------------------------------------------------------------
  static String hasher(String str) {
    var key = utf8.encode('rM81I1IcUbl0Puvwx8C7FWYAvIxXkB0G');
    var bytes = utf8.encode(str);
    var hmacSha256 = new Hmac(sha256, key);
    var digest = hmacSha256.convert(bytes);
    return hex.encode(digest.bytes).replaceAll("-", "").toUpperCase();
  }

  static String phoneParser(String phone) {
    return "(${phone.substring(0, 2)})${phone.substring(2, 5)}-${phone.substring(5, 7)}-${phone.substring(7, 9)}";
  }

  static void loading(BuildContext context) {
    showDialog(
        barrierDismissible: true,
        child: WillPopScope(
            onWillPop: () {
              return;
            },
            child: Container(
              color: Colors.transparent,
              child: Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Colors.white70),
                ),
              ),
            )),
        context: context);
  }

  static Widget simpleLoading() {
    return new Stack(
      children: [
        new Opacity(
          opacity: 0.0,
          child: const ModalBarrier(dismissible: false, color: Colors.grey),
        ),
        new Center(
          child: new CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(Colors.orange),
          ),
        ),
      ],
    );
  }

  static void noconnection(BuildContext context) {
    showDialog(
        barrierDismissible: true,
        child: WillPopScope(
          onWillPop: () {
            return;
          },
          child: NoConnectionPage(),
        ),
        context: context);
  }

  static timerStatusTick() async {
    await moveCar();
  }

  static moveCar() async {
    String result = await orderStatus(orderId, hasher(orderId));
    if (result == "Ok") {
      map.state.moveCar(orderstatus.currentLat, orderstatus.currentLong,
          orderstatus.currentDirection);
    }
  }

  static amazingLoading(BuildContext context) async {
    double a = 1;
    double b = 1;
    await showGeneralDialog(
        transitionDuration: Duration(milliseconds: 20),
        barrierColor: Color.fromRGBO(255, 255, 255, 0.1),
        barrierDismissible: false,
        context: context,
        pageBuilder: (context, a, b) {
          return WillPopScope(
            onWillPop: () {
              return;
            },
            child: Container(
              child: Container(
                child: Center(
                  child: new CircularProgressIndicator(
                    valueColor:
                        new AlwaysStoppedAnimation<Color>(Colors.orange),
                  ),
                ),
              ),
            ),
          );
        });
  }

  static Future<void> deleteOrderInfos() async {
    await SharedPref.delete("orderid");
    await SharedPref.delete("carInfo");
    await SharedPref.delete("carNumber");
    await SharedPref.delete("driverFullname");
    await SharedPref.delete("driverPhone");
    await SharedPref.delete("startlat");
    await SharedPref.delete("startlong");
    await SharedPref.delete("endlat");
    await SharedPref.delete("endlong");
    await SharedPref.delete("price");
    await SharedPref.delete("startstreet");
    await SharedPref.delete("endstreet");
  }

  static Future<void> saveOrderInfos() async {
    await SharedPref.saveV("carInfo", StaticValue.orderinfo.carInfo);
    await SharedPref.saveV("carNumber", StaticValue.orderinfo.carNumber);
    await SharedPref.saveV(
        "driverFullname", StaticValue.orderinfo.driverFullname);
    await SharedPref.saveV("driverPhone", StaticValue.orderinfo.driverPhone);
    await SharedPref.saveV(
        "startlat", StaticValue.map.state.pickup_location.latitude);
    await SharedPref.saveV(
        "startlong", StaticValue.map.state.pickup_location.longitude);
    await SharedPref.saveV(
        "endlat", StaticValue.map.state.destination_location.latitude);
    await SharedPref.saveV(
        "endlong", StaticValue.map.state.destination_location.longitude);
    await SharedPref.saveV("price", StaticValue.price);
    await SharedPref.saveV("startstreet", StaticValue.pickupStreet);
    await SharedPref.saveV("endstreet", StaticValue.destinationStreet);
  }
  static LatLng destloc;
  static LatLng picloc;
  static Future<void> getOrderInfos() async {
    var carinf = await SharedPref.getV("carInfo");
    var carnum = await SharedPref.getV("carNumber");
    var driverflnm = await SharedPref.getV("driverFullname");
    var driverphn = await SharedPref.getV("driverPhone");
    var strtlat =  double.parse(await SharedPref.getV("startlat"));
    var strtlng =double.parse( await SharedPref.getV("startlong"));
    var endlat = double.parse(await SharedPref.getV("endlat"));
    var endlng = double.parse(await SharedPref.getV("endlong"));
    var prc = double.parse(await SharedPref.getV("price"));
    var strtstrt = await SharedPref.getV("startstreet");
    var endstrt = await SharedPref.getV("endstreet");
    activeOrder= new Order(strtstrt, endstrt, strtlat, strtlng, endlat, endlng, prc);
    orderinfo=new OrderInfo(carinf, carnum, orderstatus.currentLat, orderstatus.currentLong, driverflnm, driverphn);
    pickupStreet=strtstrt;
    destinationStreet=endstrt;
    destloc=new LatLng(endlat,endlng);
    picloc= new LatLng(strtlat,strtlng);
    
  }
//-----------------------------------UTILITIES END--------------------------------------------------------------------------

//---------------------------------REQUESTS START----------------------------------

//ok.
  static Future<String> sendSMS(String phone, String token) async {
    try {
      String url = "http://api.simtoday.net/citytaxi/sms";
      var response = await http.post(
        url,
        body: json.encode({'Phone': "+994" + phone, 'Token': token}),
        headers: {HttpHeaders.contentTypeHeader: "application/json"},
      ).timeout(Duration(seconds: 5));
      if (response != null) {
        if (response.statusCode == HttpStatus.ok) {
          return "ok";
        }
      }
      return "mistake";
    } catch (e) {
      return "misxcxtake";
    }
  }

//ok.
  static Future<List<History>> history(String phone, String token) async {
    try {
      String url = "http://api.simtoday.net/citytaxi/history";
      var response = await http.post(
        url,
        body: json.encode({'PhoneNumber': phone, 'Token': token}),
        headers: {HttpHeaders.contentTypeHeader: "application/json"},
      ).timeout(Duration(seconds: 5));
      if (response != null) {
        if (response.statusCode == HttpStatus.ok) {
          List<History> histories = new List<History>();
          List<dynamic> data = json.decode(response.body);
          for (int i = 0; i < data.length; i++) {
            histories.add(new History(
                int.parse(data[i]["status"]),
                data[i]["moneyAmount"],
                data[i]["timeReg"],
                data[i]["startAddress"],
                data[i]["endAddress"],
                data[i]["driverFullName"],
                data[i]["carName"]));
          }
          histories.forEach((e) => print(e.driverFullname));
          return histories;
        }
      }
      return null;
    } catch (e) {
      return null;
    }
  }
  static BuildContext con;
  static Future<String> verifyCode(
      String phone, String code, String token) async {
    try {
      String url = "http://api.simtoday.net/citytaxi/verifycode";
      var response = await http.post(
        url,
        body: json.encode({
          'PhoneNumber': phone,
          'Code': code,
          'Token': StaticValue.hasher(phone)
        }),
        headers: {HttpHeaders.contentTypeHeader: "application/json"},
      ).timeout(Duration(seconds: 5));
      if (response != null) {
        if (response.statusCode == HttpStatus.ok) {
          return "ok";
        }
        return "notok";
      }
      return "mistake";
    } catch (e) {
      return "mistake";
    }
  }

//ok.
  static Future<String> checkUser(String phone, String token) async {
    try {
      String url = "http://api.simtoday.net/citytaxi/checkuser";
      var response = await http.post(
        url,
        body: json.encode({'Phone': phone, 'Token': token}),
        headers: {HttpHeaders.contentTypeHeader: "application/json"},
      ).timeout(Duration(seconds: 5));
      if (response != null) {
        if (response.statusCode == HttpStatus.ok) {
          if (response.body.toString() == "YES") {
            isCoop = true;
          }
        }
        return "ok";
      }
      print("just return error");
      return "mistake";
    } catch (e) {
      print("just catch error");
      return "mistake";
    }
  }

//ok
  static Future<String> calculatePrice(
      double sLat, double sLong, double fLat, double fLong) async {
    try {
      String url = "http://api.simtoday.net/citytaxi/calculate";
      var response = await http.post(
        url,
        body: json.encode({
          'SLat': sLat.toString(),
          'SLong': sLong.toString(),
          'FLat': fLat.toString(),
          'FLong': fLong.toString()
        }),
        headers: {HttpHeaders.contentTypeHeader: "application/json"},
      ).timeout(Duration(seconds: 5));
      if (response != null) {
        if (response.statusCode == HttpStatus.ok) {
          return response.body
              .replaceAll("\"", "")
              .replaceAll("{", "")
              .replaceAll("}", "")
              .replaceAll(":", "")
              .replaceAll("money", "")
              .toString();
        }
        return "3";
      }
      return "mistake";
    } catch (e) {
      return "mistake";
    }
  }

//ok.
  static Future<String> order(
      String startStreet,
      String finishStreet,
      String phone,
      String token,
      double slat,
      double slong,
      double flat,
      double flong,
      int type) async {
        print(startStreet);
        print(finishStreet);
        print(phone);
        print(token);
        print(slat);
        print(slong);
        print(flat);
        print(flong);
        print(type);


    if (isOrdered) {
      try {
        String url = "http://api.simtoday.net/citytaxi/order";
        var response = await http.post(
          url,
          body: json.encode({
            'SStreet': startStreet,
            'SLat': slat.toString(),
            'SLong': slong.toString(),
            'FStreet': finishStreet,
            'FLat': flat.toString(),
            'FLong': flong.toString(),
            'PhoneNumber': phone,
            'Token': token,
            'Type': type
          }),
          headers: {HttpHeaders.contentTypeHeader: "application/json"},
        ).timeout(Duration(seconds: 5));
        if (response != null) {
          print(response.body);
          if (response.statusCode == HttpStatus.ok) {
            orderId = json.decode(response.body)["id"];
            return "ok";
          }
          print("order mistake"+" "+response.statusCode.toString());
          return "notok";
        }
        print("order mistake");
        return "mistake";
      } catch (e) {
        print("order mistake 1");
        return "mistake";
      }
    }
    return "cancled";
  }

//ok.
  static Future<String> orderInfo(String orderid, String token) async {
    try {
      String url = "http://api.simtoday.net/citytaxi/orderinfo";
      var response = await http.post(
        url,
        body: json.encode({'OrderId': orderid, 'Token': token}),
        headers: {HttpHeaders.contentTypeHeader: "application/json"},
      ).timeout(Duration(seconds: 5));
      if (response != null) {
        print(response.body);
        if (response.statusCode == HttpStatus.ok) {
          var data = json.decode(response.body);
          orderinfo = new OrderInfo(
              data["carInfo"],
              data["carNumber"],
              double.parse(data["carLat"]),
              double.parse(data["carLong"]),
              data["driverFullname"],
              data["driverPhone"]);
          return "ok";
        }
        return "notok";
      }
      return "mistake";
    } catch (e) {
      return "mistake";
    }
  }

//ok
  static Future<String> cancel(
      String phone, String orderid, String token) async {
    try {
      String url = "http://api.simtoday.net/citytaxi/cancel";
      var response = await http.post(
        url,
        body: json
            .encode({'PhoneNumber': phone, 'OrderId': orderid, 'Token': token}),
        headers: {HttpHeaders.contentTypeHeader: "application/json"},
      ).timeout(Duration(seconds: 5));

      if (response != null) {
        if (response.statusCode == HttpStatus.ok) {
          return "ok";
        }
      }
      return "mistake";
    } catch (e) {
      return "mistake";
    }
  }

//ok
  static Future<String> orderStatus(String orderid, String token) async {
    try {
      String url = "http://api.simtoday.net/citytaxi/currentstatus";
      var response = await http.post(
        url,
        body: json.encode({'OrderId': orderid, 'Token': token}),
        headers: {HttpHeaders.contentTypeHeader: "application/json"},
      ).timeout(Duration(seconds: 5));

      if (response != null) {
        if (response.statusCode == HttpStatus.ok) {
          var data = json.decode(response.body);
          orderstatus = new OrderStatus(data["currentDirection"],
              data["currentLat"], data["currentLong"], data["status"]);
          return "ok";
        } else {
          return "notok";
        }
      }
      return "mistake";
    } catch (e) {
      return "mistake";
    }
  }

//This is other weed
  static Future<Order> getAddress(String orderid, String token) async {
    String url = "http://api.simtoday.net/citytaxi/getaddress";
    var response = await http.post(
      url,
      body: json.encode({'OrderId': orderid, 'Token': token}),
      headers: {HttpHeaders.contentTypeHeader: "application/json"},
    );
    print("GetAddress: " + response.statusCode.toString());
    if (response.statusCode == HttpStatus.ok) {
      await orderInfo(orderid, token);
      var data = json.decode(response.body);
      activeOrder = new Order(
          data["startStreet"],
          data["endStreet"],
          data["startLat"],
          data["startLong"],
          data["endLong"],
          data["endtLong"],
          data["money"]);

      pickupStreet = activeOrder.startStreet;
      destinationStreet = activeOrder.endStreet;

      return activeOrder;
    }
    return null;
  }

//---------------------------------REQUESTS END----------------------------------

}
