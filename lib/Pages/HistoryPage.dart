import 'package:flutter/material.dart';
import 'package:citytaxi/CustomWidgets/ResponsiveContainer.dart';
import 'package:citytaxi/ViewModels/HistoryViewModel.dart';
import 'package:citytaxi/Behaviors/CustomScrolBehavior.dart';

class HistoryPage extends StatefulWidget {
  List<HistoryViewModel> _histories;
  HistoryPage(this._histories);
  @override
  State<StatefulWidget> createState() {
    return _HistoryPageState(_histories);
  }
}

class _HistoryPageState extends State<HistoryPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<HistoryViewModel> _histories;
  HistoryListBuilder(BuildContext context, int index) => this._histories[index];
  _HistoryPageState(this._histories);
  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: new Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text(
            "Gediş Tarixçəsi",
            style: new TextStyle(
              color: Colors.black54,
            ),
          ),
        ),
        body: new Container(
          decoration: BoxDecoration(
            color: Color.fromRGBO(255, 255, 255, 0.95),
            image: DecorationImage(
              image: AssetImage("assets/background.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: new Center(
            child: Container(
              color: Colors.white,
              child: new ResponsiveContainer(
                heightPercent: 100,
                widthPercent: 92,
                child: new Container(
                  padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.02,
                    right: MediaQuery.of(context).size.height * 0.02,
                  ),
                  child: new ScrollConfiguration(
                    behavior: CustomScrolBehavior(),
                    child: _histories.length > 0
                        ? new ListView.separated(
                            separatorBuilder: (context, index) => Divider(
                                  color: Colors.grey[500],
                                  indent: 15,
                                ),
                            itemCount: this._histories.length,
                            itemBuilder: (BuildContext context, int index) =>
                                HistoryListBuilder(context, index),
                          )
                        : Container(
                            child: Center(
                              child: Text("Gediş tarixçəniz boşdur."),
                            ),
                          ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
