import 'package:citytaxi/Models/History.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:responsive_container/responsive_container.dart';
import 'package:citytaxi/Behaviors/CustomScrolBehavior.dart';

class RideInfoPage extends StatefulWidget {
  History history;
  RideInfoPage(this.history);
  @override
  _RideInfoPageState createState() => _RideInfoPageState(history);
}

class _RideInfoPageState extends State<RideInfoPage> {
  History history;
  _RideInfoPageState(this.history);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: new Scaffold(
        body: Stack(
          children: <Widget>[
            Container(
                decoration: BoxDecoration(
                  color: Colors.orange,
                  image: DecorationImage(
                      image: AssetImage("assets/orngbck.png"),
                      fit: BoxFit.fitHeight),
                ),
                height: MediaQuery.of(context).size.height * 0.25,
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.06,
                            right: MediaQuery.of(context).size.height * 0.2),
                        child: Center(
                            child: Text(
                          DateFormat('d MMM, yyyy  kk:mm')
                              .format(DateTime.parse(this.history.time)),
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                              fontWeight: FontWeight.w500),
                        )),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.02,
                            right: MediaQuery.of(context).size.height * 0.22),
                        child: Center(
                            child: Text(
                          history.carName,
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: 13,
                              color: Colors.white70,
                              fontWeight: FontWeight.w500),
                        )),
                      )
                    ],
                  ),
                )),
            Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.25),
              decoration: BoxDecoration(
                color: Colors.transparent,
                image: DecorationImage(
                    image: AssetImage("assets/background.png"),
                    fit: BoxFit.cover),
              ),
              child: Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.02,
                  left: MediaQuery.of(context).size.width * 0.1,
                  right: MediaQuery.of(context).size.width * 0.1,
                  bottom: MediaQuery.of(context).size.height * 0.25,
                ),
                child: Container(
                    decoration: BoxDecoration(
                      boxShadow: <BoxShadow>[
                        new BoxShadow(
                          color: Colors.grey[400],
                          offset: new Offset(0.2, 0.2),
                          blurRadius: 5.0,
                        ),
                      ],
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                    ),
                    child: Container(
                      child: new ResponsiveContainer(
                        heightPercent: 100,
                        widthPercent: 92,
                        child: new Container(
                          padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.02,
                            right: MediaQuery.of(context).size.height * 0.02,
                          ),
                          child: new ScrollConfiguration(
                            behavior: CustomScrolBehavior(),
                            child: RideWidget(this.history),
                          ),
                        ),
                      ),
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class RideWidget extends StatefulWidget {
  History history;
  RideWidget(this.history);
  @override
  State<StatefulWidget> createState() {
    return RideWidgetState(this.history);
  }
}

class RideWidgetState extends State<RideWidget> {
  double _tileMarginLeft;
  double _tileMarginTop;
  History history;
  RideWidgetState(this.history);
  @override
  Widget build(BuildContext context) {
    this._tileMarginLeft = MediaQuery.of(context).size.width * 0.04;
    this._tileMarginTop = MediaQuery.of(context).size.height * 0.013;
    return Column(
      children: <Widget>[
        new Container(
          margin: EdgeInsets.only(
            left: this._tileMarginLeft,
            top: this._tileMarginTop,
          ),
          alignment: Alignment.centerLeft,
          child: new Text(
            "Sürücü",
            textAlign: TextAlign.left,
            style: new TextStyle(fontSize: 12, color: Colors.black54),
          ),
        ),
        new Container(
          margin: EdgeInsets.only(
            left: this._tileMarginLeft,
            top: this._tileMarginTop,
          ),
          alignment: Alignment.centerLeft,
          child: new Text(
            history.driverFullname,
            textAlign: TextAlign.left,
            style: new TextStyle(
              fontSize: 14,
            ),
          ),
        ),
        Container(
          height: 0.3,
          color: Colors.black45,
          margin: EdgeInsets.symmetric(
              vertical: MediaQuery.of(context).size.height * 0.008,
              horizontal: MediaQuery.of(context).size.width * 0.03),
        ),
        new Container(
          margin: EdgeInsets.only(
            left: this._tileMarginLeft,
            top: this._tileMarginTop,
          ),
          alignment: Alignment.centerLeft,
          child: new Text(
            "Başlanğıc",
            textAlign: TextAlign.left,
            style: new TextStyle(fontSize: 12, color: Colors.black54),
          ),
        ),
        new Container(
          margin: EdgeInsets.only(
            left: this._tileMarginLeft,
            top: this._tileMarginTop,
          ),
          alignment: Alignment.centerLeft,
          child: new Text(
            history.startAddress,
            textAlign: TextAlign.left,
            style: new TextStyle(
              fontSize: 14,
            ),
          ),
        ),
        Container(
          height: 0.3,
          color: Colors.black45,
          margin: EdgeInsets.symmetric(
              vertical: MediaQuery.of(context).size.height * 0.008,
              horizontal: MediaQuery.of(context).size.width * 0.03),
        ),
        new Container(
          margin: EdgeInsets.only(
            left: this._tileMarginLeft,
            top: this._tileMarginTop,
          ),
          alignment: Alignment.centerLeft,
          child: new Text(
            "Dayanacaq",
            textAlign: TextAlign.left,
            style: new TextStyle(fontSize: 12, color: Colors.black54),
          ),
        ),
        new Container(
          margin: EdgeInsets.only(
            left: this._tileMarginLeft,
            top: this._tileMarginTop,
          ),
          alignment: Alignment.centerLeft,
          child: new Text(
            history.endAddress,
            textAlign: TextAlign.left,
            style: new TextStyle(
              fontSize: 14,
            ),
          ),
        ),
        Container(
          height: 0.3,
          color: Colors.black45,
          margin: EdgeInsets.only(
              top: MediaQuery.of(context).size.height * 0.008,
              bottom: MediaQuery.of(context).size.height * 0.03,
              left: MediaQuery.of(context).size.width * 0.03,
              right: MediaQuery.of(context).size.width * 0.03),
        ),
        Center(
          child: Column(
            children: <Widget>[
              Center(
                child: Text(
                  "Ümumi Məbləğ:",
                  style: TextStyle(color: Colors.black45),
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.025,
                ),
              ),
              Center(
                child: Text(
                  history.money.toString() + " " + "AZN",
                  style: TextStyle(
                      color: Colors.red,
                      fontSize: 19,
                      fontWeight: FontWeight.w600),
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}
