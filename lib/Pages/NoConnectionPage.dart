import 'package:flutter/material.dart';

class NoConnectionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          child: Container(
            alignment: Alignment.center,
            child: Image.asset("assets/no_connection.png"),
          ),
        ),
      ),
    );
  }
}
