
import 'package:citytaxi/CustomWidgets/MapControl.dart';
import 'package:citytaxi/CustomWidgets/TaxiDrawer.dart';
import 'package:citytaxi/CustomWidgets/TopSection.dart';
import 'package:flutter/material.dart';
import 'package:citytaxi/Static/StaticValue.dart';
import 'package:citytaxi/CustomWidgets/BottomSection.dart';
import 'package:citytaxi/CustomWidgets/MapHome.dart';

class MapPage extends StatefulWidget {
  MapPage() {
    StaticValue.mapPageState = new MapPageState();
  }
  @override
  State<StatefulWidget> createState() {
    return StaticValue.mapPageState;
  }
}

class MapPageState extends State<MapPage> {
  List<Widget> widgets = new List<Widget>();
  MediaQueryData queryData;

  bool isOrdered;
  bool isActive = false;
  checkValue(value) async {
    if (value != null) {}
  }


  @override
  void initState() {
    StaticValue.map = new MapControl();
    StaticValue.map.createState();
    
    if(StaticValue.orderId!=null){
    widgets.add(StaticValue.map);
    widgets.add(TopSection());
    widgets.add(BottomSection(2));
    }
    else{
    widgets.add(StaticValue.map);
    widgets.add(MapHome());
    isOrdered = false;
    }
    StaticValue.mapPageState = this;
    super.initState();
  }

  removeAll() {
    setState(() {
      print("---------------------------");
      print("Mappage removeall");
      print("---------------------------");
      widgets.clear();
      widgets.add(StaticValue.map);
      widgets.add(MapHome());
    });
  }

  refreshMap() {
    StaticValue.map.state.removeAll();
  }

  void removeWidget(Widget widget, int choosen) {
    setState(() {
      widgets.clear();
      widgets.add(StaticValue.map);
      widgets.add(widget);
      widgets.add(BottomSection(choosen));
    });
  }

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
  @override
  Widget build(BuildContext context) {
    StaticValue.con=context;
    // StaticValue.subscription = Connectivity()
    //     .onConnectivityChanged
    //     .listen((ConnectivityResult result) {
    //   print("-----------");
    //   print(result.index);
    //   if (result.index == 2 && StaticValue.isConnected) {
    //     StaticValue.noconnection(context);
    //     StaticValue.isConnected = false;
    //   } else {
    //     if (!StaticValue.isConnected) {
    //       StaticValue.isConnected = true;
    //       Navigator.of(context).pop();
    //     }
    //   }
    // });
    queryData = MediaQuery.of(context);
    widgets.add(Padding(
      padding: EdgeInsets.only(
        top: MediaQuery.of(context).size.width * 0.03,
        left: MediaQuery.of(context).size.width * 0.03,
      ),
      child: GestureDetector(
        onTap: () {
          _scaffoldKey.currentState.openDrawer();
        },
        child: Icon(
          Icons.menu,
          color: Color.fromARGB(255, 112, 112, 112),
          size: 30,
        ),
      ),
    ));
    return WillPopScope(
      onWillPop: () {
        return;
      },
      child: SafeArea(
        child: Scaffold(
          key: _scaffoldKey,
          drawer: TaxiDrawer(),
          body: Stack(children: widgets),
        ),
      ),
    );
  }
}
