import 'package:citytaxi/Pages/NoConnectionPage.dart';
import 'package:citytaxi/Static/StaticValue.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:citytaxi/CustomWidgets/PinValidate.dart';
import 'package:citytaxi/CustomWidgets/PhoneValidate.dart';

class LoginPage extends StatefulWidget {
  @override
  createState() => new LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  int childWidget = 0;
  MediaQueryData queryData;
  var phone;

  @override
  Widget build(BuildContext context) {
    // StaticValue.subscription = Connectivity()
    //     .onConnectivityChanged
    //     .listen((ConnectivityResult result) {
    //   if (result.index == 2) {
    //     StaticValue.isConnected = false;
    //     Navigator.push(
    //       context,
    //       MaterialPageRoute(builder: (context) => NoConnectionPage()),
    //     );
    //   } else {
    //     if (!StaticValue.isConnected) {
    //       Navigator.of(context).pop();
    //     }
    //   }
    // });
    queryData = MediaQuery.of(context);
    return new SafeArea(
      child: new Scaffold(
        resizeToAvoidBottomPadding: false,
        body: new Container(
          decoration: BoxDecoration(
            color: Color.fromRGBO(255, 255, 255, 0.95),
            image: DecorationImage(
                image: AssetImage("assets/background.png"), fit: BoxFit.cover),
          ),
          child: Center(
            child: Container(
              padding: EdgeInsets.symmetric(
                  vertical: queryData.size.height * 0.22,
                  horizontal: queryData.size.width * 0.1),
              child: Container(
                  decoration: BoxDecoration(
                    boxShadow: <BoxShadow>[
                      new BoxShadow(
                        color: Colors.grey[400],
                        offset: new Offset(0.2, 0.2),
                        blurRadius: 5.0,
                      ),
                    ],
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white,
                  ),
                  child: (childWidget == 0)
                      ? new PhoneValidate(this)
                      : new PinValidate(this, this.phone)),
            ),
          ),
        ),
      ),
    );
  }
}
